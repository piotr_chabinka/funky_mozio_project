from django.urls import include, path

from service_area.views import (
    ListCreateServiceAreaView,
    RetrieveUpdateDestroyServiceAreaView,
    ListServiceAreasWithPointView
)

urlpatterns = [
    path('', ListCreateServiceAreaView.as_view()),
    path('<int:pk>', RetrieveUpdateDestroyServiceAreaView.as_view()),
    path('list_containing', ListServiceAreasWithPointView.as_view())
]
