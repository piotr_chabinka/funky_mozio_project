from rest_framework.serializers import ModelSerializer

from service_area.models import ServiceArea, Price


class PriceSerializer(ModelSerializer):

    class Meta:
        model = Price
        fields = ('value', 'currency')


class ServiceAreaSerializer(ModelSerializer):

    price = PriceSerializer()

    class Meta:
        model = ServiceArea
        fields = ('name', 'price', 'area', 'provider')

    def create(self, validated_data):
        price, _ = Price.objects.get_or_create(
            value=validated_data['price']['value'],
            currency=validated_data['price']['currency']
        )

        return ServiceArea.objects.create(
            price=price,
            name=validated_data['name'],
            area=validated_data['area'],
            provider=validated_data['provider']
        )

    def update(self, instance, validated_data):
        price, _ = Price.objects.get_or_create(
            value=validated_data['price']['value'],
            currency=validated_data['price']['currency']
        )

        instance.name = validated_data['name']
        instance.price = price
        instance.area = validated_data['area']
        instance.provider = validated_data['provider']
        instance.save()

        return instance
