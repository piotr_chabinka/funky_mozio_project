from django.contrib.gis.geos import Point
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView

from service_area.models import ServiceArea
from service_area.serializers import ServiceAreaSerializer


class ListCreateServiceAreaView(ListCreateAPIView):
    queryset = ServiceArea.objects.select_related(
        'provider',
        'provider__user',
        'provider__language',
        'provider__currency',
        'price'
    ).all()
    serializer_class = ServiceAreaSerializer


class RetrieveUpdateDestroyServiceAreaView(RetrieveUpdateDestroyAPIView):
    queryset = ServiceArea.objects.select_related(
        'provider',
        'provider__user',
        'provider__language',
        'provider__currency',
        'price'
    ).all()
    serializer_class = ServiceAreaSerializer
    http_method_names = ('get', 'put', 'patch', 'delete')


class ListServiceAreasWithPointView(ListAPIView):
    serializer_class = ServiceAreaSerializer
    http_method_names = ('get',)

    def get_queryset(self):
        print(self.request.GET)
        return ServiceArea.objects.select_related(
            'provider',
            'provider__user',
            'provider__language',
            'provider__currency',
            'price'
        ).filter(
            area__contains=Point(
                x=int(self.request.GET.get('longitude', 0)),
                y=int(self.request.GET.get('latitude', 0))
            )
        )
