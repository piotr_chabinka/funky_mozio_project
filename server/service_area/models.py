from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _

from provider.models import Currency, Provider


class Price(models.Model):
    currency = models.ForeignKey(Currency, default=Currency.USD, on_delete=models.SET_DEFAULT)
    value = models.IntegerField(_('value'), blank=False, null=False, default=0)

    def __str__(self):
        return f'{self.value} {self.currency}'


class ServiceArea(models.Model):

    name = models.CharField(_('name'), max_length=30, blank=False, null=False)
    price = models.ForeignKey(Price, default=0, on_delete=models.SET_DEFAULT)
    area = models.PolygonField(_('area'))
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} ({self.price}, {self.area}, {self.provider})'