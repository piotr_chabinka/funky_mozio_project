from django.urls import include, path

from provider.views import (
    CreateProviderView,
    CreateListUserView,
    RetrieveUpdateDestroyProviderView,
    RetrieveUpdateDestroyUserView
)

urlpatterns = [
    path('user/', CreateListUserView.as_view()),
    path('user/<int:pk>', RetrieveUpdateDestroyUserView.as_view()),
    path('', CreateProviderView.as_view(), name='create_provider'),
    path('<int:pk>', RetrieveUpdateDestroyProviderView.as_view())
]
