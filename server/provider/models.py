from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.is_active = True
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):

    objects = UserManager()

    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
    )

    email = models.EmailField(_('email address'), unique=True)
    name = models.CharField(_('name'), max_length=30, blank=False, null=False, unique=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    phone_number = models.CharField(validators=[phone_regex], null=True, blank=True, max_length=17)
    is_staff = models.BooleanField(_('staff'), default=False)
    is_superuser = models.BooleanField(_('superuser'), default=False)

    USERNAME_FIELD = 'name'
    REQUIRED_FIELDS = ['email']


class Language(models.Model):

    EN = 'EN'
    PL = 'PL'

    LANGUAGES = (
        (EN, 'EN'),
        (PL, 'PL'),
    )

    value = models.CharField(max_length=max(len(l[1]) for l in LANGUAGES), choices=LANGUAGES, default=EN)

    def __str__(self):
        return self.value


class Currency(models.Model):

    USD = 'USD'
    EUR = 'EUR'
    PLN = 'PLN'

    CURRENCIES = (
        (USD, 'USD'),
        (EUR, 'EUR'),
        (PLN, 'PLN')
    )

    value = models.CharField(max_length=max(len(c[1]) for c in CURRENCIES), choices=CURRENCIES, default=USD)

    def __str__(self):
        return self.value


class Provider(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    language = models.ForeignKey(Language, default=Language.EN, on_delete=models.SET_DEFAULT)
    currency = models.ForeignKey(Currency, default=Currency.USD, on_delete=models.SET_DEFAULT)

    def __str__(self):
        return f'{self.user} ({self.language}, {self.currency})'
