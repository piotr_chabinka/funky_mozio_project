from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from provider.models import Provider, User
from provider.serializers import ProviderSerializer, UserSerializer


class CreateProviderView(ListCreateAPIView):
    queryset = Provider.objects.select_related('user', 'language', 'currency').all()
    serializer_class = ProviderSerializer


class CreateListUserView(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class RetrieveUpdateDestroyProviderView(RetrieveUpdateDestroyAPIView):
    queryset = Provider.objects.select_related('user', 'language', 'currency').all()
    serializer_class = ProviderSerializer
    http_method_names = ('get', 'put', 'patch', 'delete')


class RetrieveUpdateDestroyUserView(RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    http_method_names = ('get', 'put', 'patch', 'delete')
