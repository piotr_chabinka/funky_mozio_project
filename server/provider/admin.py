from django.contrib import admin

from provider.models import User, Language, Currency, Provider


admin.site.register(User)
admin.site.register(Language)
admin.site.register(Currency)
admin.site.register(Provider)
