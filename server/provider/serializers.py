from rest_framework.serializers import ModelSerializer

from provider.models import Currency, Language, User, Provider


class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'name', 'phone_number')


class LanguageSerializer(ModelSerializer):

    class Meta:
        model = Language
        fields = '__all__'


class CurrencySerializer(ModelSerializer):

    class Meta:
        model = Currency
        fields = '__all__'


class ProviderSerializer(ModelSerializer):

    class Meta:
        model = Provider
        fields = '__all__'
