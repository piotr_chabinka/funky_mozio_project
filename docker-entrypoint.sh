#!/bin/bash
set -e

if [[ "$1" = 'server' ]]; then
    ls
    cd server

    python manage.py migrate --no-input

    mkdir -p ./logs
    touch ./logs/gunicorn.log
    touch ./logs/gunicorn-access.log
    tail -n 0 -f ./logs/gunicorn*.log &

    gunicorn --bind 0.0.0.0:8000 server.wsgi:application --log-level=info --log-file=./logs/gunicorn.log --access-logfile=./logs/gunicorn-access.log -w 1 -k uvicorn.workers.UvicornH11Worker

else
    echo "You need to specify command which will be run by entrypoint!"
fi

exec "$@"