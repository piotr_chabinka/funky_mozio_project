FROM python:3.7-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN apt-get update && apt-get install -y build-essential python3.7-dev libpq-dev libgdal-dev
RUN pip install -r requirements.txt

COPY docker-entrypoint.sh /
RUN chmod u+x /docker-entrypoint.sh

EXPOSE 8000
ENTRYPOINT ["/docker-entrypoint.sh"]
